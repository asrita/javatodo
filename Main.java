//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Task {
    private String description;
    private String date;
    private boolean completed;

    public Task(String description, String date) {
        this.description = description;
        this.date = date;
        this.completed = false;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void markAsCompleted() {
        this.completed = true;
    }

    @Override
    public String toString() {
        return "Task{" +
                "description='" + description + '\'' +
                ", date='" + date + '\'' +
                ", completed=" + completed +
                '}';
    }
}

class TaskManager {
    private List<Task> tasks;

    public TaskManager() {
        this.tasks = new ArrayList<>();
    }
    public List<Task> displayTask(){
        return tasks;
    }
    public void addTask(Task task) {
        tasks.add(task);
    }

    public Map<String, List<Task>> retrieveTasksGroupedByDate() {
        Map<String, List<Task>> groupedTasks = new HashMap<>();

        for (Task task : tasks) {
            groupedTasks.computeIfAbsent(task.getDate(), k -> new ArrayList<>()).add(task);
        }

        return groupedTasks;
    }

    public void markTaskAsCompleted(Task task) {
        task.markAsCompleted();
    }

    public List<Task> searchTasks(String keyword) {
        List<Task> matchingTasks = new ArrayList<>();

        for (Task task : tasks) {
            if (task.getDescription().startsWith(keyword)) {
                matchingTasks.add(task);
            }
        }

        return matchingTasks;
    }
    public void deleteTask(int taskNo) {
        try{
            tasks.remove(taskNo);
        }catch(IndexOutOfBoundsException e){
            System.out.println("Enter correct index");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        TaskManager taskManager = new TaskManager();

        Scanner input = new Scanner(System.in);
        String choice;

        do {
            System.out.println("Please choose an option");
            System.out.println("1. Display all tasks ");
            System.out.println("2. Add a task ");
            System.out.println("3. Group by date ");
            System.out.println("4. Mark task as completed ");
            System.out.println("5. Tasks starting with keyword ");
            System.out.println("6. Delete the task ");
            System.out.println("7. Exit");
            choice = input.nextLine();

            switch (choice) {
                case "1":
                    System.out.println("Displaying all tasks ");
                    System.out.println(taskManager.displayTask());
                    break;
                case "2":
                    System.out.println("Enter a task ");
                    String userInput = input.nextLine();
                    String dateInput= input.nextLine();
                    taskManager.addTask(new Task(userInput,dateInput));
                    break;
                case "3":
                    System.out.println("Grouping the Tasks ");
                    Map<String, List<Task>> groupedTasks = taskManager.retrieveTasksGroupedByDate();
                    System.out.println("Tasks grouped by date: " + groupedTasks);
                    break;
                case "4":
                    System.out.println("Enter description of the task ");
                    String taskDescriptionToMarkCompleted = input.nextLine();
                    List<Task> tasksToMarkCompleted = taskManager.searchTasks(taskDescriptionToMarkCompleted);
                    if (!tasksToMarkCompleted.isEmpty()) {
                        for (Task task : tasksToMarkCompleted) {
                            taskManager.markTaskAsCompleted(task);
                            System.out.println("Task marked as completed: " + task);
                        }
                    } else {
                        System.out.println("No matching tasks found with the specified description.");
                    }
                    // taskManager.markTaskAsCompleted(taskToComplete);
                    // System.out.println("Task marked as completed: " + taskToComplete);
                    break;
                case "5":
                    System.out.println("Tasks starting with keyword ");
                    String keyword = input.nextLine();
                    List<Task> searchedTasks = taskManager.searchTasks(keyword);
                    System.out.println("Tasks starting with 'Task': " + searchedTasks);
                    break;
                case "6":
                    System.out.println("Enter the index of the task");
                    int taskNo = input.nextInt();
                    input.nextLine();
                    taskManager.deleteTask(taskNo);
                    break;
                case "7":
                    System.out.println("Exiting the program ");
                    break;
                default:
                    System.out.println("Invalid choice. Please enter a number between 1 and 6.");
                    break;
            }

        } while (!choice.equals("7"));

        input.close();
    }
}